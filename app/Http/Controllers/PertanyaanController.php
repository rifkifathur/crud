<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create()
    {
    	return view('template.CRUD.CreateQuestion');
    }

    public function index()
    {
    	return view('template.CRUD.ListQuestions');
    }

    public function store(Request $request)
    {	
    	$request->validate([
    		'title' => 'required|unique:pertanyaan',
    		'body'  => 'required'
    	]);
    	$query = DB::table('pertanyaan')->insert([
    		"judul" => $request["title"],
    		"isi" 	=> $request["body"]
    	]);
    	return redirect('/pertanyaan/create');
    }
}
