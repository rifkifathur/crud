<?php

	Route::get('/', 'HomeController@home');
	Route::get('/data-tables', 'AuthController@data');
	Route::get('/pertanyaan/create', 'PertanyaanController@create');
	// Route::get('/pertanyaan', 'PertanyaanController@index');
	Route::post('/pertanyaan', 'PertanyaanController@store');
	// Route::get('/pertanyaan', 'PertanyaanController@show');
	// Route::get('/pertanyaan', 'PertanyaanController@edit');
	// Route::put('/pertanyaan', 'PertanyaanController@update');
	// Route::delete('/pertanyaan', 'PertanyaanController@destroy');
?>